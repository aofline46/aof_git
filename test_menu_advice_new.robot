*** Settings ***
Library           SeleniumLibrary
#Library           Collections

*** Variables ***
${Chrome_Open}            https://www.advice.co.th/
${Chrome_Browser}         headlesschrome   #Chrome
#{Chrome_Browser}         Chrome


*** Test Cases ***
Open Browser Advice
    Open Browser          ${Chrome_Open}             ${Chrome_Browser}
   # Set Browser Implicit Wait  10
Set_Window_Size
    #Maximize Browser Window
    #Set Browser Implicit Wait  10
    Set Window Size    1920    1080

Check_Menu_NOTEBOOK
    Mouse Down          xpath://*[@id="primary_nav_wrap"]/ul/li[1]/a
    Set Browser Implicit Wait  2

    #เช็คเมนูย่อย 2 Notebook   

    Element Text Should Be          //*[@id="menu-left-notebook"]/a[1]          Notebook HUAWEI
    Element Text Should Be          //*[@id="menu-left-notebook"]/a[2]          Notebook ACER
    Element Text Should Be          //*[@id="menu-left-notebook"]/a[3]          Notebook ASUS
    Element Text Should Be          //*[@id="menu-left-notebook"]/a[4]          Notebook LENOVO
    Element Text Should Be          //*[@id="menu-left-notebook"]/a[5]          Notebook MSI
    Element Text Should Be          //*[@id="menu-left-notebook"]/a[6]          Notebook HP
    Element Text Should Be          //*[@id="menu-left-notebook"]/a[7]          Notebook DELL
    Element Text Should Be          //*[@id="menu-left-notebook"]/a[8]          Notebook ACER ConceptD


Check_Menu_NOTEBOOK_Gaming

#เช็คเมนูย่อย1 Notebook Gaming
    Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[2]/div/div[1]/div[1]/a          Notebook Gaming

#คลิก + Notebook Gaming
    Click Element          xpath://*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[2]/div/div[1]/div[1]/i
    Set Browser Implicit Wait  2

#เช็คเมนูย่อย 2 Notebook Gaming
    Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[1]          Notebook Gaming ACER
    Set Browser Implicit Wait  10

    Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[2]          Notebook Gaming ASUS
    Set Browser Implicit Wait  10

    #Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[3]          Notebook Gaming GIGABYTE
    #Set Browser Implicit Wait  10

    #Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[4]          Notebook Gaming LENOVO
    #Set Browser Implicit Wait  10

    #Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[5]          Notebook Gaming MSI
    #Set Browser Implicit Wait  10

    #Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[6]          Notebook Gaming HP
    #Set Browser Implicit Wait  10

    #Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[7]          Notebook Gaming DELL
    #Set Browser Implicit Wait  10


Check_Menu_NOTEBOOK_2in1

#เช็คเมนูย่อย1 Notebook 2in1
    Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[3]/div/div[1]/div[1]/a          Notebook 2in1

#คลิก + Notebook 2in1
    Click Element          xpath://*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[3]/div/div[1]/div[1]/i
    Set Browser Implicit Wait  2

#เช็คเมนูย่อย 2 Notebook 2in1
   Element Text Should Be          //*[@id="menu-left-notebook-2in1"]/a[1]          Notebook 2in1 LENOVO
   Set Browser Implicit Wait  5
   #Element Text Should Be          //*[@id="menu-left-notebook-2in1"]/a[2]         Notebook 2in1 ASUS
   #Set Browser Implicit Wait  2

#เช็คเมนูย่อย1  MicroSoft Surface
    Element Text Should Be              //*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[4]/div/div[1]/div/a          MicroSoft Surface


#คลิก + MicroSoft Surface
    Click Element          xpath://*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[4]/div/div[1]/div/i
    Set Browser Implicit Wait  2

#เช็คเมนูย่อย 2 Notebook MICROSOFT SURFACE
    Element Text Should Be          //*[@id="menu-left-microsoft-surface"]/a          Notebook MICROSOFT SURFACE
    Set Browser Implicit Wait  2

    Close All Browsers



###

